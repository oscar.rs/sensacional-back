class ProductClient{
    constructor(){}



    dbInformation(){
        return {
            user: 'mega',
            host: 'localhost',
            database: 'test_sensacional',
            password: 'mega',
            port: 5432,
        }
    }

    sections(){
        return 'SELECT CATEGORIES FROM PRODUCTS GROUP BY CATEGORIES';
    }

    brands(){
        return "SELECT BRAND FROM PRODUCTS GROUP BY BRAND ORDER BY BRAND";
    }

    conditions(){
        return "SELECT CONDITION FROM PRODUCTS GROUP BY CONDITION";
    }

    filter(filterData){
        let query = "SELECT * FROM PRODUCTS WHERE "
        Object.keys(filterData).forEach( (key, index) =>{
            if (key !== 'categories' && key !== 'name'){
                query +=  key + '=' + `'${filterData[key]}'` + ' ' 
            }else{
                if(key === 'categories'){
                    query +=  key + ' ILIKE ' +  `'%${filterData[key]}%'` + ' '
                }else{
                    let filterNames = filterData[key].split(' ');
                    filterNames.forEach( (nameFilter, index) => {
                        query +=  key + ' ILIKE ' +  `'%${nameFilter}%'` + ' '
                        if(filterNames.length - 1 !== index) query += "AND "
                    })
                }
            }
            if ( Object.keys(filterData).length - 1 !== index ) query += "AND "
        })
        query += ' ORDER BY PRICE'
        return query;
    }


    separatePrices(arrayProducts){
        var begin = 0;
        var results = {};

        if(arrayProducts.length){
            if (arrayProducts[0].price < begin) {
                begin = 0 ;
            }else{
                begin = arrayProducts[0].price - (arrayProducts[0].price % 1000)
            }


            while(begin <= arrayProducts[arrayProducts.length-1].price){
                let array = this.checkRange(begin, begin + 999, arrayProducts);
                if(array.length){
                    let key = 'range ' + begin + ' - ' + (parseInt(begin) + 999);
                    results[key] = array; 
                }
                begin+= 1000; 
            }
        }
        return results;
    }

    checkRange(begin, finish, products){
        let array = products.filter((product) =>{
            return product.price>= begin && product.price <=finish
        })
        return array;
    }
}
module.exports = ProductClient;