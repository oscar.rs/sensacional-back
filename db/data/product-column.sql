
CREATE TABLE public.products
(
  sku character varying,
  imagen character varying,
  id integer,
  name character varying,
  brand character varying,
  precio_ref money,
  price money,
  qty character varying,
  status character varying,
  stock_availability character varying,
  created_at date,
  categories character varying,
  empty_b character varying,
  empty_c character varying,
  condition character varying,
  empty_e character varying,
  empty_f character varying,
  empty_g character varying,
  empty_h character varying
)

