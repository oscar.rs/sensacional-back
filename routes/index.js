var express = require('express');
var Product = require('../db/product');
var productClient = new Product();
var router = express.Router();
const { Client } = require('pg')
var pg = require('pg');
const client = new Client(productClient.dbInformation())
client.connect()

router.get('/sections', function(req, respon, next){
  var results = [];
  client.query(productClient.sections(), (err,res) =>{
      if (err) {
      console.log(err.stack)
    } else {
      res.rows.forEach(row => {
        let category = row.categories.split('/');
        results.push(category[category.length-1]);
      })
      return respon.json({sections: results.filter( (el, i, arr) => arr.indexOf(el) === i)});
    }
  })
})


router.get('/brands', function(req, respon, next){
  var results = [];

  client.query(productClient.brands(), (err,res) =>{
      if (err) {
      console.log(err.stack)
    } else {
      res.rows.forEach(row => results.push(row.brand))
      return respon.json({brands: results});
    }
  })
})

router.get('/conditions', function(req, respon, next){
  var results = [];

  client.query(productClient.conditions(), (err,res) =>{
      if (err) {
      console.log(err.stack)
    } else {
      res.rows.forEach(row => results.push(row.condition))
      return respon.json({conditions: results});
    }
  })
})


router.get('/filter', function(req, respon, next){
  var results = [];

  client.query(productClient.filter(req.query), (err,res) =>{
      if (err) {
      console.log(err.stack)
    } else {
      res.rows.forEach(row => {
        row.price = parseFloat(row.price.replace('$','').split('.')[0].replace(',', ''));
        results.push(row)
      })
      return respon.json({products: productClient.separatePrices(results)});
    }
  });
})


module.exports = router;
